package org.itschool.hillel;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/index.html")
public class HelloWorldServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String processedId = id + "!!!";

        req.setAttribute("processedId", processedId);
        req.setAttribute("currentTime", new Date());

        req.getRequestDispatcher("/WEB-INF/hello.jsp").forward(req, resp);
    }
}
